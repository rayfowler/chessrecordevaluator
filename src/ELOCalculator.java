
public class ELOCalculator {
	public double calculate(int w, int l, int d) {
		int g = w + l + d;
		double winPct = (w + d/2.0)  / g;
		double elo = -Math.log10(1.0/winPct-1.0)*400;
		return elo;
	}
}
