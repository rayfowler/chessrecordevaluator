import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class Main {
	public static void main(String[] args) {
    	JFrame frame = new JFrame("Chess Record Evaluator");
    	frame.addWindowListener(new WindowAdapter() {
    		@Override
    		public void windowClosing(WindowEvent e) { System.exit(0); }
    	});	 
    	frame.setLayout(new BorderLayout());
    	frame.setSize(1024, 768);
    	frame.add(new MainUI(), BorderLayout.CENTER);
    	frame.setResizable(true);
    	frame.setVisible(true);
    }
}
