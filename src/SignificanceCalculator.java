import java.math.BigDecimal;

public class SignificanceCalculator {
	static BigDecimal[] factorials;
	public double calculate(int w, int l, int d) {
		int g = w+l+d;
		double pDraw = (double) d/ g;
		initFactorials(g);
				
		int HI = Math.max(w,l);
		int LO = Math.min(w, l);
		double minValue = 0.00001/g;
		double sumProb = 0.0;
		for (int w0=HI; w0 <= g; w0++) {
			for (int l0=LO; l0 >= 0; l0--) {
				int d0 = g - w0 - l0;
				if (d0 >= 0) {
					double newProb = probability(w0, l0, d0, pDraw);
					sumProb += newProb;
					// short-circuit loop when values get excessively small
					if (newProb < minValue) {
						// if the first L value is too small, then we also done with W values
						if (l0 == l)
							w0 = g;			
						l0 = 0;
					}
				}
			}
		}
		return 1.0 - sumProb;
	}
	private double probability(int w, int l, int d, double pDraw) {
		// this calculates a multinomial probablity of w-l-d with
		// expected draw pct of pDraw and equal w/l probabilities
		//System.out.println("sign: +"+w+"-"+l+"="+d);
		int g = w+l+d;
		double pWin = (1.0-pDraw)/2;
		double pLoss = pWin;
		
		BigDecimal fact1 = fact(g).divide(fact(w).multiply(fact(l)).multiply(fact(d)));
		double fact2 = Math.pow(pWin, w)*Math.pow(pLoss, l)*Math.pow(pDraw, d);
		//System.out.println("prob: "+fact1+" * "+ fact2);
		return fact1.multiply(BigDecimal.valueOf(fact2)).doubleValue();
	}
	private BigDecimal fact(int i) {
		return factorials[i];
	}
	private void initFactorials(int n) {
		if ((factorials != null) && (factorials.length > n))
			return;
		
		//System.out.println("init factorials");
		
		BigDecimal[] newFactorials = new BigDecimal[n+1];
		BigDecimal[] oldFactorials = factorials;
		
		int lastIndex = 0;
		if (oldFactorials != null) {
			lastIndex = oldFactorials.length;
			for (int i=0;i<lastIndex;i++)
				newFactorials[i]=oldFactorials[i];
		}
		for (int i=lastIndex;i<=n;i++) {
			if (i <= 1)
				newFactorials[i] = BigDecimal.ONE;
			else
				newFactorials[i] = newFactorials[i-1].multiply(BigDecimal.valueOf(i));
		}		
		factorials = newFactorials;
	}
}
