import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MainUI extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1557476120434306598L;
	private Font textFont = new Font("TimesRoman", Font.PLAIN, 24);
	JLabel winLbl, lossLbl, drawLbl, losLbl, eloLbl;
	JTextField winsFld, lossesFld, drawsFld, signFld, eloFld;
	int wins, losses, draws;
	JButton calculate;
	SignificanceCalculator signCalculator = new SignificanceCalculator();
	ELOCalculator eloCalculator = new ELOCalculator();
	DecimalFormat eloFmt = new DecimalFormat("+#;-#");
	DecimalFormat signFmt = new DecimalFormat("#.######");
	
	public MainUI() {
		setBackground(Color.black);
		setBorder(BorderFactory.createEmptyBorder(100,300,100,300));
		winLbl = label("Wins");
		lossLbl = label("Losses");
		drawLbl = label("Draws");
		losLbl = label("Significance");
		eloLbl = label("ELO");
		winsFld = textInput();
		lossesFld = textInput();
		drawsFld = textInput();
		signFld = textDisplay();
		eloFld = textDisplay();
		
		calculate = button("Calculate");
		
		JPanel dataPanel = new JPanel();
		dataPanel.setOpaque(false);
		GridLayout dataLayout = new GridLayout(5,2);
		dataLayout.setHgap(20);
		dataLayout.setVgap(20);
		dataPanel.setLayout(dataLayout);
		dataPanel.add(winLbl);
		dataPanel.add(winsFld);
		dataPanel.add(lossLbl);
		dataPanel.add(lossesFld);
		dataPanel.add(drawLbl);
		dataPanel.add(drawsFld);
		dataPanel.add(losLbl);
		dataPanel.add(signFld);
		dataPanel.add(eloLbl);
		dataPanel.add(eloFld);
		
		BorderLayout mainLayout = new BorderLayout(0,70);
		setLayout(mainLayout);
		add(title("Chess Record Evaluator"), BorderLayout.NORTH);
		add(dataPanel, BorderLayout.CENTER);
		add(calculate, BorderLayout.SOUTH);
	}
	private JLabel title(String text) {
		JLabel lbl= new JLabel(text);
		lbl.setFont(new Font("TimesRoman", Font.PLAIN, 36));
		lbl.setForeground(Color.white);
		lbl.setHorizontalAlignment(JLabel.CENTER);
		return lbl;
	}
	private JLabel label(String text) {
		JLabel lbl= new JLabel(text);
		lbl.setFont(textFont);
		lbl.setForeground(Color.lightGray);
		lbl.setHorizontalAlignment(JLabel.RIGHT);
		return lbl;
	}
	private JTextField textInput() {
		JTextField fld = new JTextField();
		fld.setFont(textFont);
		return fld;
	}
	private JTextField textDisplay() {
		JTextField fld = textInput();
		fld.setEnabled(false);
		fld.setDisabledTextColor(Color.black);
		return fld;
	}
	private JButton button(String text) {
		JButton button = new JButton(text);
		button.setPreferredSize(new Dimension(getWidth(),50));
		button.setFont(textFont);
		button.addActionListener(this);
		return button;
	}
	private void calculate() {
		signFld.setText("");
		eloFld.setText("");
		
		if (validated()) {
			signFld.setText(signFmt.format(signCalculator.calculate(wins,losses,draws)));
			eloFld.setText(eloFmt.format(eloCalculator.calculate(wins,losses,draws)));
		}
	}
	private boolean validated() {
		boolean valid = true;
	    try { 
	    	resetError(winsFld);
	    	wins = winsFld.getText().trim().isEmpty() ? 0 : Integer.parseInt(winsFld.getText().trim()); 
	    	winsFld.setText(""+wins);
	    } 
	    catch(Exception e) {
	    	markError(winsFld);
	    	valid = false;  
	    }
	    
	    try { 
	    	resetError(lossesFld);
	    	losses = lossesFld.getText().trim().isEmpty() ? 0 : Integer.parseInt(lossesFld.getText().trim());
	    	lossesFld.setText(""+losses);
	    } 
	    catch(Exception e) {
	    	markError(lossesFld);
	    	valid = false;  
	    }
	    
	    try { 
	    	resetError(drawsFld);
	    	draws = drawsFld.getText().trim().isEmpty() ? 0 : Integer.parseInt(drawsFld.getText().trim());
	    	drawsFld.setText(""+draws);
	    } 
	    catch(Exception e) {
	    	markError(drawsFld);
	    	valid = false;  
	    }
	    return valid;
	}
	private void resetError(JTextField f) {
		f.setForeground(Color.black);
	}
	private void markError(JTextField f) {
		signFld.setText("Input Error!");
		eloFld.setText("");
		f.setForeground(Color.red);
	}
	@Override
	public void actionPerformed(ActionEvent ev) {
		if (ev.getSource() == calculate)
			calculate();
	}

}
